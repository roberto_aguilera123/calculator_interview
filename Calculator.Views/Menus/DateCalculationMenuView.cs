﻿using Calculator.Views.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;

namespace Calculator.Views.Menus
{
    public class DateCalculationMenuView : Base
    {
        public DateCalculationMenuView()
        {
            aeDesktop = AutomationElement.RootElement;
            aeCalculator = GetCalculatorWindow();
        }

        private readonly string GroupClassName = "LandmarkTarget";
        private readonly string DateCalculationOptionIdCombo = "DateCalculationOption";
        private readonly string DateTextId = "DateText";
        private readonly string DateDiff_FromDateId = "DateDiff_FromDate";
        private readonly string DateDiff_ToDateId = "DateDiff_ToDate";
        private readonly string CalendarPopupClassName = "Popup";
        private readonly string CalendarViewId = "CalendarView";
        private readonly string HeaderCalendarId = "HeaderButton";
        private readonly string HeaderDecadeViewScrollViewerCalendarId = "DecadeViewScrollViewer";
        private readonly string YearViewScrollViewerId = "YearViewScrollViewer";
        private readonly string MonthViewScrollViewerId = "MonthViewScrollViewer";
        private readonly string DateDiffAllUnitsResultLabelId = "DateDiffAllUnitsResultLabel";
    
        /// <summary>
        /// Sets the menu of calculator to difference between dates
        /// </summary>
        public void SetDifferenceBetweenDates()
        {
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement groupList = FindChildByClassName(childCalculatorWindow, GroupClassName);
            groupList = FindChildById(groupList, DateCalculationOptionIdCombo);
            SetSelectElementByName(groupList, "Difference between dates");
        }

        /// <summary>
        /// Sets a date From
        /// </summary>
        /// <param name="date"></param>
        public void SetDateFrom(DateTime date)
        {
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement dateFrom = FindChildByClassName(childCalculatorWindow, GroupClassName);
            ClickOnElement(dateFrom, DateDiff_FromDateId);
            childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement calendarGroup = FindChildByClassName(childCalculatorWindow, CalendarPopupClassName);
            calendarGroup = FindChildByClassName(calendarGroup, "Flyout");
            AutomationElement calendarView = FindChildById(calendarGroup, CalendarViewId);
            ///CLicks 3 times in header to get to year
            for (int i = 0; i < 2; i++)
            {
                ClickOnElement(calendarView, HeaderCalendarId);
                calendarView = FindChildById(calendarGroup, CalendarViewId);
            }
            ///CLicks on year needs to test with bigger year
            childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            calendarGroup = FindChildByClassName(childCalculatorWindow, CalendarPopupClassName);
            calendarGroup = FindChildByClassName(calendarGroup, "Flyout");
            AutomationElement calendarViewMenu = FindChildById(calendarGroup, CalendarViewId); 
            calendarView = FindChildById(calendarViewMenu, HeaderDecadeViewScrollViewerCalendarId);
            ClickOnElementByName(calendarView, date.Year.ToString());
            ///CLicks on month
            childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            calendarGroup = FindChildByClassName(childCalculatorWindow, CalendarPopupClassName);
            calendarGroup = FindChildByClassName(calendarGroup, "Flyout");
            calendarViewMenu = FindChildById(calendarGroup, CalendarViewId);
            calendarView = FindChildById(calendarViewMenu, YearViewScrollViewerId);
            string formatMonth = date.ToString("MMMM");
            formatMonth = char.ToUpper(formatMonth[0]) + formatMonth.Substring(1);
            ClickOnElementByName(calendarView, formatMonth);

            ///Clicks on day
            childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            calendarGroup = FindChildByClassName(childCalculatorWindow, CalendarPopupClassName);
            calendarGroup = FindChildByClassName(calendarGroup, "Flyout");
            calendarViewMenu = FindChildById(calendarGroup, CalendarViewId);
            calendarViewMenu = FindChildById(calendarViewMenu, MonthViewScrollViewerId);         
            var allDays = FindChildByAllClassName(calendarViewMenu, "CalendarViewDayItem");

            foreach (AutomationElement element in allDays)
            {
                // Get a TableItemPattern from the source of the event.
                TableItemPattern tableItemPattern =
                    GetTableItemPattern(element);
                // Get a TablePattern from the container of the current element.
                TablePattern tablePattern =
                    GetTablePattern(tableItemPattern.Current.ContainingGrid);
                AutomationElement tableItem = null;
                tableItem = tablePattern.GetItem(
                tableItemPattern.Current.Row,
                tableItemPattern.Current.Column);
                AutomationElement[] rowHeaders = tableItemPattern.Current.GetRowHeaderItems();

                string yearAndMonth = rowHeaders[0].Current.Name;
                formatMonth = date.ToString("MMMM");
                formatMonth = char.ToUpper(formatMonth[0]) + formatMonth.Substring(1);
                string todayYearAndMonth = formatMonth +" " + date.ToString("yyyy");
                string test = element.Current.Name;
                string test2 = date.Day.ToString();
                if (string.Equals(todayYearAndMonth.ToLower(), todayYearAndMonth.ToLower())
                    && string.Equals(element.Current.Name, date.Day.ToString()))
                {
                    SelectionItemPattern selectionItemPattern;
                    selectionItemPattern =
                    element.GetCurrentPattern(
                    SelectionItemPattern.Pattern) as SelectionItemPattern;
                    selectionItemPattern.Select();
                    return;
                }
                Console.WriteLine(element.Current.Name+ " "+ rowHeaders[0].Current.Name + "Not found");
            }
        }
        
        /// <summary>
        /// Set date To from calculator
        /// </summary>
        /// <param name="date"></param>
        public void SetDateTo(DateTime date)
        {
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement dateFrom = FindChildByClassName(childCalculatorWindow, GroupClassName);
            ClickOnElement(dateFrom, DateDiff_ToDateId);
            childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement calendarGroup = FindChildByClassName(childCalculatorWindow, CalendarPopupClassName);
            calendarGroup = FindChildByClassName(calendarGroup, "Flyout");
            AutomationElement calendarView = FindChildById(calendarGroup, CalendarViewId);
            ///CLicks 3 times in header to get to year
            for (int i = 0; i < 2; i++)
            {
                ClickOnElement(calendarView, HeaderCalendarId);
                calendarView = FindChildById(calendarGroup, CalendarViewId);
            }
            ///CLicks on year needs to test with bigger year
            childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            calendarGroup = FindChildByClassName(childCalculatorWindow, CalendarPopupClassName);
            calendarGroup = FindChildByClassName(calendarGroup, "Flyout");
            AutomationElement calendarViewMenu = FindChildById(calendarGroup, CalendarViewId);
            calendarView = FindChildById(calendarViewMenu, HeaderDecadeViewScrollViewerCalendarId);
            ClickOnElementByName(calendarView, date.Year.ToString());
            ///CLicks on month
            childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            calendarGroup = FindChildByClassName(childCalculatorWindow, CalendarPopupClassName);
            calendarGroup = FindChildByClassName(calendarGroup, "Flyout");
            calendarViewMenu = FindChildById(calendarGroup, CalendarViewId);
            calendarView = FindChildById(calendarViewMenu, YearViewScrollViewerId);
            string formatMonth = date.ToString("MMMM");
            formatMonth = char.ToUpper(formatMonth[0]) + formatMonth.Substring(1);
            ClickOnElementByName(calendarView, formatMonth);

            ///Clicks on day
            childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            calendarGroup = FindChildByClassName(childCalculatorWindow, CalendarPopupClassName);
            calendarGroup = FindChildByClassName(calendarGroup, "Flyout");
            calendarViewMenu = FindChildById(calendarGroup, CalendarViewId);
            calendarViewMenu = FindChildById(calendarViewMenu, MonthViewScrollViewerId);
            var allDays = FindChildByAllClassName(calendarViewMenu, "CalendarViewDayItem");

            foreach (AutomationElement element in allDays)
            {
                // Get a TableItemPattern from the source of the event.
                TableItemPattern tableItemPattern =
                    GetTableItemPattern(element);
                // Get a TablePattern from the container of the current element.
                TablePattern tablePattern =
                    GetTablePattern(tableItemPattern.Current.ContainingGrid);
                AutomationElement tableItem = null;
                tableItem = tablePattern.GetItem(
                tableItemPattern.Current.Row,
                tableItemPattern.Current.Column);
                AutomationElement[] rowHeaders = tableItemPattern.Current.GetRowHeaderItems();

                string yearAndMonth = rowHeaders[0].Current.Name;
                formatMonth = date.ToString("MMMM");
                formatMonth = char.ToUpper(formatMonth[0]) + formatMonth.Substring(1);
                string todayYearAndMonth = formatMonth + " " + date.ToString("yyyy");
                string test = element.Current.Name;
                string test2 = date.Day.ToString();
                if (string.Equals(todayYearAndMonth.ToLower(), todayYearAndMonth.ToLower())
                    && string.Equals(element.Current.Name, date.Day.ToString()))
                {
                    SelectionItemPattern selectionItemPattern;
                    selectionItemPattern =
                    element.GetCurrentPattern(
                    SelectionItemPattern.Pattern) as SelectionItemPattern;
                    selectionItemPattern.Select();
                    return;
                }
                Console.WriteLine(element.Current.Name + " " + rowHeaders[0].Current.Name + "Not found");
            }
        }

        /// <summary>
        /// Verifies the dates difference
        /// </summary>
        /// <param name="daysDifference"></param>
        /// <returns></returns>
        public bool IsDateDifferenceCorrect(int daysDifference)
        {
            DateTime thisDay = DateTime.Today;
            DateTime futureDate = thisDay.AddDays(daysDifference);
            TimeSpan diffence = futureDate - thisDay;
            // Get result from calculator
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement groupList = FindChildByClassName(childCalculatorWindow, GroupClassName);
            string resultsFull = FindChildById(groupList, DateDiffAllUnitsResultLabelId).Current.Name;
            if (resultsFull.Contains("week") || resultsFull.Contains("weeks") || resultsFull.Contains("month")
                   || resultsFull.Contains("months") || resultsFull.Contains("year") || resultsFull.Contains("years"))
            {
                //get days from second display
                string daysDiffResult = FindChildByName(groupList, diffence.Days.ToString() + " days").Current.Name;
                string extractdays = new String(daysDiffResult.Where(Char.IsDigit).ToArray());
                // convert result to days weeks
                int days,years, weeks;
                years = (Convert.ToInt32(extractdays) / 365);
                if (years == 0) { }
                else
                {
                    if (!(resultsFull.Contains(years.ToString() + " year"))) return false;
                }
                weeks = (Convert.ToInt32(extractdays) % 365) / 7;
                if (weeks == 0) { }
                else
                {
                    if (!(resultsFull.Contains(weeks.ToString() + " week"))) return false;
                }
                days = Convert.ToInt32(extractdays) - ((years * 365) + (weeks * 7));
                if (days == 0) { }
                else
                {
                    if (!(resultsFull.Contains(days.ToString() + " day"))) return false;
                }
                /// all days
                if (Convert.ToInt32(extractdays) == diffence.Days) return true;
                else return false;
            }
            else
            {
                /// since is less than a week the calculator doesnt show weeks and the other menu under
                string extractdays = new String(resultsFull.Where(Char.IsDigit).ToArray());
                if (Convert.ToInt32(extractdays) == diffence.Days) return true;
                else return false;
            }
        }

        ///--------------------------------------------------------------------
        /// <summary>
        /// Obtains a TablePattern control pattern from an
        /// AutomationElement.
        /// </summary>
        /// <param name="targetControl">
        /// The AutomationElement of interest.
        /// </param>
        /// <returns>
        /// A TablePattern object.
        /// </returns>
        ///--------------------------------------------------------------------
        private TablePattern GetTablePattern(
            AutomationElement targetControl)
        {
            TablePattern tablePattern = null;

            try
            {
                tablePattern =
                    targetControl.GetCurrentPattern(
                    TablePattern.Pattern)
                    as TablePattern;
            }
            // Object doesn't support the
            // TablePattern control pattern
            catch (InvalidOperationException)
            {
                return null;
            }

            return tablePattern;
        }

        ///--------------------------------------------------------------------
        /// <summary>
        /// Obtains a TableItemPattern control pattern from an
        /// AutomationElement.
        /// </summary>
        /// <param name="targetControl">
        /// The AutomationElement of interest.
        /// </param>
        /// <returns>
        /// A TableItemPattern object.
        /// </returns>
        ///--------------------------------------------------------------------
        private TableItemPattern GetTableItemPattern(
            AutomationElement targetControl)
        {
            TableItemPattern tableItemPattern = null;

            try
            {
                tableItemPattern =
                    targetControl.GetCurrentPattern(
                    TableItemPattern.Pattern)
                    as TableItemPattern;
            }
            // Object doesn't support the
            // TableItemPattern control pattern
            catch (InvalidOperationException)
            {
                return null;
            }

            return tableItemPattern;
        }
    }
}
