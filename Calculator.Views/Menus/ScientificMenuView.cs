﻿using Calculator.Views.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;

namespace Calculator.Views.Menus
{
    public class ScientificMenuView : Base
    {
        public ScientificMenuView()
        {
            aeDesktop = AutomationElement.RootElement;
            aeCalculator = GetCalculatorWindow();
        }

        /// <summary>
        /// Calculates log from a random number
        /// </summary>
        public string CalculateBaseLogFroMNumber(string num)
        {
            Pad pad = new Pad();
            foreach (char c in num)
            { 
            pad.SetNumberPad(c.ToString());
            }
            pad.SetScientificFunction("log");
            return GetResultsText();
        }

        /// <summary>
        /// Check if result from log base 10 operation match the calculator displaye
        /// </summary>
        /// <param name="number"></param>
        /// <param name="displayResult"></param>
        /// <returns></returns>
        public bool IsBasicLogResultsDisplayCorrect(string number,string displayResult )
        {
            double result = Math.Log10(double.Parse(number));
            double _displayResult = double.Parse(displayResult);
            if (result == _displayResult) return true;
            else
             return false;
        }
    }
}
