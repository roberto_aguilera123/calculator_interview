﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;
using Calculator.Views.Controllers;

namespace Calculator.Views.Menu
{
    public class NavigationMenu : Base
    {
        public NavigationMenu()
        {
            aeDesktop = AutomationElement.RootElement;
            aeCalculator = GetCalculatorWindow();
        }

        private readonly string CustomId = "NavView";
        private readonly string TogglePaneId = "TogglePaneButton";
        private readonly string ProgrammerId = "Programmer";
        private readonly string RootListId = "PaneRoot";
        private readonly string MenuListId = "MenuItemsHost";
        private readonly string ScrollViewerListClassName = "ScrollViewer";
        private readonly string ScientificId = "Scientific";
        private readonly string DateId = "Date";
        private readonly string StandardId = "Standard";


        /// <summary>
        /// Navigate to programmer menu
        /// </summary>
        public void GoToProgrammerMode()
        {
            NavigateMenu(ProgrammerId);
        }

        /// <summary>
        /// Go to scientific mode
        /// </summary>
        public void GoToScientificMode()
        {
            NavigateMenu(ScientificId);

        }

        /// <summary>
        /// go to date calculation
        /// </summary>
        public void GoToDateCalcuation()
        {
            NavigateMenu(DateId);
        }

        /// <summary>
        /// /Navigates to standard mode
        /// </summary>
        public void GoToStandard()
        {
            NavigateMenu(StandardId);
        }
        /// <summary>
        /// private menu to reuse for menu navigation
        /// </summary>
        /// <param name="menu"></param>
        private void NavigateMenu(string menu)
        {
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement customWindowList = FindChildById(childCalculatorWindow, CustomId);
            ClickOnElement(customWindowList, TogglePaneId);
            customWindowList = FindChildById(customWindowList, RootListId);
            customWindowList = FindChildByClassName(customWindowList, ScrollViewerListClassName);
            customWindowList = FindChildById(customWindowList, MenuListId);
            ClickOnElement(customWindowList, menu);
        }
    }
}
