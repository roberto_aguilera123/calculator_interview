﻿using Calculator.Views.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;

namespace Calculator.Views.Menus
{
    public class StandardCalculatorMenuView: Base
    {
        public StandardCalculatorMenuView()
        {
            aeDesktop = AutomationElement.RootElement;
            aeCalculator = GetCalculatorWindow();
        }

        private readonly string GroupClassName = "LandmarkTarget";
        private readonly string HistoryMemoryListsId = "DockPanel";
        private readonly string HistoryLabelId = "HistoryLabel";
        private readonly string HistoryListViewId = "HistoryListView";
        private readonly string MemoryLabelId = "MemoryLabel";
        private readonly string MemoryListViewId = "MemoryListView";
        private readonly string MemoryItemValueId = "MemoryItemValue";


        /// <summary>
        /// returns the value of two numbers and verifies the result in calculator display
        /// </summary>
        /// <returns></returns>
        public int SumTwoNumbers(int a, int b)
        {
            int result;
            result = a + b;
            Pad pad = new Pad();
            foreach (char c in a.ToString())
            {
                pad.SetNumberPad(c.ToString());
            }
            pad.SetStandardPad("sum");
            foreach (char c in b.ToString())
            {
                pad.SetNumberPad(c.ToString());
            }
            pad.SetStandardPad("equals");
            string displayResult = GetResultsText().Replace(",", "");
            if (displayResult.Contains(result.ToString())) return result;
            else throw new Exception("Sum results are incorrect");
        }

        /// <summary>
        /// Clears results
        /// </summary>
        public void ClearCalculator()
        {
            Pad pad = new Pad();
            pad.SetStandardControlPad("clear");
        }

        /// <summary>
        /// Click retrievie value from memory
        /// </summary>
        public void RetrieveValueFromMemory()
        {
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement groupList = FindChildByClassName(childCalculatorWindow, GroupClassName);
            groupList = FindChildById(groupList, HistoryMemoryListsId);
            groupList = FindChildById(groupList, "DockPivot");
            SetSelectElement(groupList, MemoryLabelId);
            Pad pad = new Pad();
            pad.SetMemoryControl("memory recall");
        }

        /// <summary>
        /// Clicks first item from history
        /// </summary>
        public void ClickFirstItemFromHistory()
        {
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement groupList = FindChildByClassName(childCalculatorWindow, GroupClassName);
            groupList = FindChildById(groupList, HistoryMemoryListsId);
            groupList = FindChildById(groupList, "DockPivot");
            SetSelectElement(groupList, HistoryLabelId);
            AutomationElement tableListMemory = FindChildByClassName(childCalculatorWindow, GroupClassName);
            tableListMemory = FindChildById(tableListMemory, HistoryMemoryListsId);
            tableListMemory = FindChildById(tableListMemory, "DockPivot");
            tableListMemory = FindChildById(tableListMemory, HistoryLabelId);
            tableListMemory = FindChildById(tableListMemory, HistoryListViewId);
            var listHistory = FindChildByAllClassName(tableListMemory,"ListViewItem");

            InvokePattern buttonPattern =
           (InvokePattern)listHistory[0].GetCurrentPattern(InvokePattern.Pattern);
            //Once get the pattern then calling Invoke method on that
            buttonPattern.Invoke();
        }

        /// <summary>
        /// verifies if values are in display result and expression
        /// </summary>
        /// <param name="valueOne"></param>
        /// <param name="valueTwo"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public bool DoesDisplayResultContains(string valueOne, string valueTwo, string result)
        {
            string resultExpression = GetCalculationExpressionText();
            if (!(resultExpression.Contains(valueOne))) return false;
            if (!(resultExpression.Contains(valueTwo))) return false;
            string displayResult = GetResultsText().Replace(",", "");
            if (displayResult.Contains(result.ToString())) return true;
            else return false;
        }

        /// <summary>
        /// CLick save in memory
        /// </summary>
        public void SaveInMemory()
        {
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement groupList = FindChildByClassName(childCalculatorWindow, GroupClassName);
            groupList = FindChildById(groupList, HistoryMemoryListsId);
            groupList = FindChildById(groupList, "DockPivot");
            SetSelectElement(groupList, MemoryLabelId);
            Pad pad = new Pad();
            pad.SetMemoryControl("memory store");
        }

        /// <summary>
        /// Gets the value from memory
        /// </summary>
        /// <returns></returns>
        public string GetMemoryValue()
        {
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement tableListMemory = FindChildByClassName(childCalculatorWindow, GroupClassName);
            tableListMemory = FindChildById(tableListMemory, HistoryMemoryListsId);
            tableListMemory = FindChildById(tableListMemory, "DockPivot");
            tableListMemory = FindChildById(tableListMemory, MemoryLabelId);
            tableListMemory = FindChildById(tableListMemory, MemoryListViewId);
            var listMemory = FindChildByAllClassName(tableListMemory, "ListViewItem");
            var valueSaved=  FindChildById(listMemory[0], MemoryItemValueId);
            return valueSaved.Current.Name.Replace(",","");
        }

        /// <summary>
        /// Clicks memory add
        /// </summary>
        public void ClickMemoryAdd()
        {
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement groupList = FindChildByClassName(childCalculatorWindow, GroupClassName);
            groupList = FindChildById(groupList, HistoryMemoryListsId);
            groupList = FindChildById(groupList, "DockPivot");
            SetSelectElement(groupList, MemoryLabelId);
            Pad pad = new Pad();
            pad.SetMemoryControl("memory add");
        }
    }
}
