﻿using Calculator.Views.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;

namespace Calculator.Views.Menus
{
    public class Pad : Base
    {
        public Pad()
        {
            aeDesktop = AutomationElement.RootElement;
            aeCalculator = GetCalculatorWindow();
        }

        private readonly string groupClassName = "LandmarkTarget";
        private readonly string ScientificFunctionsName = "Scientific functions";
        private readonly string NumberPadId = "NumberPad";
        private readonly string StandardOperatorsId = "StandardOperators";
        private readonly string StandardControlsId = "DisplayControls";
        private readonly string MemoryPanelId = "MemoryPanel";


        /// <summary>
        /// Sets a number pad 
        /// </summary>
        /// <param name="value"></param>
        public void SetNumberPad(string value)
        {
            string id = string.Empty;
            switch (value.ToLower())
            {
                case "a":
                    id = "aButton";
                    break;

                case "b":
                    id = "bButton";
                    break;
                case "c":
                    id = "cButton";
                    break;
                case "d":
                    id = "dButton";
                    break;
                case "e":
                    id = "eButton";
                    break;
                case "f":
                    id = "fButton";
                    break;
                case "1":
                    id = "num1Button";
                    break;
                case "2":
                    id = "num2Button";
                    break;
                case "3":
                    id = "num3Button";
                    break;
                case "4":
                    id = "num4Button";
                    break;
                case "5":
                    id = "num5Button";
                    break;
                case "6":
                    id = "num6Button";
                    break;
                case "7":
                    id = "num7Button";
                    break;
                case "8":
                    id = "num8Button";
                    break;
                case "9":
                    id = "num9Button";
                    break;
                case "0":
                    id = "num0Button";
                    break;
                case ".":
                    id = "decimalSeparatorButton";
                    break;
                default:
                    Console.WriteLine($"Not found in pad");
                    break;
            }
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement numberPadList = FindChildByClassName(childCalculatorWindow, groupClassName);
            numberPadList = FindChildById(numberPadList, NumberPadId);
            ClickOnElement(numberPadList, id);
        }

        /// <summary>
        /// Clicks on standard pad operation menu
        /// </summary>
        /// <param name="operation"></param>
        public void SetStandardPad(string operation)
        {
            string id = string.Empty;
            switch (operation.ToLower())
            {
                case "divide":
                    id = "divideButton";
                    break;

                case "multiply":
                    id = "multiplyButton";
                    break;
                case "minus":
                    id = "minusButton";
                    break;
                case "sum":
                    id = "plusButton";
                    break;
                case "equals":
                    id = "equalButton";
                    break;
                default:
                    Console.WriteLine($"operation not found in pad");
                    break;
            }
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement standardOperators = FindChildByClassName(childCalculatorWindow, groupClassName);
            standardOperators = FindChildById(standardOperators, StandardOperatorsId);
            ClickOnElement(standardOperators, id);
        }

        /// <summary>
        /// Sets the a value from the scentific pad
        /// </summary>
        /// <param name="value"></param>
        public void SetScientificFunction(string value)
        {
            string id = string.Empty;
            switch (value.ToLower())
            {
                case "square":
                    id = "xpower2Button";
                    break;
                case "square root":
                    id = "squareRootButton";
                    break;
                case "x to the exponent":
                    id = "powerButton";
                    break;
                case "ten to the exponent":
                    id = "powerOf10Button";
                    break;
                case "log":
                    id = "logBase10Button";
                    break;
                case "log base":
                    id = "logBaseEButton";
                    break;
                default:
                    Console.WriteLine($"Not found in pad");
                    break;
            }
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement scientificPadList = FindChildByClassName(childCalculatorWindow, groupClassName);
            scientificPadList = FindChildByName(scientificPadList, ScientificFunctionsName);
            ClickOnElement(scientificPadList, id);
        }

        /// <summary>
        /// Sets standard pard control
        /// </summary>
        /// <param name="operation"></param>
        public void SetStandardControlPad(string operation)
        {
            string id = string.Empty;
            switch (operation.ToLower())
            {
                case "percent":
                    id = "percentButton";
                    break;
                case "clear entry":
                    id = "clearEntryButton";
                    break;
                case "clear":
                    id = "clearButton";
                    break;
                case "backspace":
                    id = "backspaceButton";
                    break;
                default:
                    Console.WriteLine($"operation not found in pad");
                    break;
            }
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement standardControl = FindChildByClassName(childCalculatorWindow, groupClassName);
            standardControl = FindChildById(standardControl, StandardControlsId);
            ClickOnElement(standardControl, id);
        }

        /// <summary>
        /// Sets memory control
        /// </summary>
        /// <param name="operation"></param>
        public void SetMemoryControl(string operation)
        {
            string id = string.Empty;
            switch (operation.ToLower())
            {
                case "clear memory":
                    id = "ClearMemoryButton";
                    break;
                case "memory recall":
                    id = "MemRecall";
                    break;
                case "memory add":
                    id = "MemPlus";
                    break;
                case "memory minus":
                    id = "MemMinus";
                    break;
                case "memory store":
                    id = "memButton";
                    break;
                default:
                    Console.WriteLine($"operation not found in pad");
                    break;
            }
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement memoryControl = FindChildByClassName(childCalculatorWindow, groupClassName);
            memoryControl = FindChildById(memoryControl, MemoryPanelId);
            ClickOnElement(memoryControl, id);
        }

    }
}
