﻿using Calculator.Views.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;

namespace Calculator.Views.Menus
{
    public class ProgrammerMenuVIew : Base
    {
        public ProgrammerMenuVIew()
            {
                aeDesktop = AutomationElement.RootElement;
                aeCalculator = GetCalculatorWindow();
            }

        private readonly string GroupClassName = "LandmarkTarget";
        private readonly string ProgrammerOperatorsId = "ProgrammerOperators";
        private readonly string resultDecimalId = "decimalButton";
        private readonly string ResultHexId = "hexButton";
        private readonly string ResultBinId = "binaryButton";
        private readonly string ResultOctId = "octolButton";




        /// <summary>
        /// Sets teh operator based on data type  HEX, DEC, OCT, and BIN
        /// </summary>
        /// <param name="menu"></param>
        public void SetOperator(string selectOperator)
        {
            string selection = string.Empty;
            switch (selectOperator.ToLower())
            {
                case "hex":
                    selection = "hexButton";
                    break;

                case"dec":
                    selection = "decimalButton";
                    break;

                case"oct":
                    selection = "octolButton";
                    break;
                case "bin":
                    selection = "binaryButton";
                    break;
                default:
                    Console.WriteLine($"Not found in menu");
                    break;
            }

            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement groupList = FindChildByClassName(childCalculatorWindow, GroupClassName);
            groupList = FindChildById(groupList, ProgrammerOperatorsId);
            SetSelectElement(groupList, selection);
        }

        /// <summary>
        /// sets a random number based on operator
        /// </summary>
        /// <param name="selectOperator"></param>
        public void SetRandomNumberBasedOnOperator(string selectOperator)
        {
            string random = string.Empty;
            string selection = string.Empty;
            switch (selectOperator.ToLower())
            {
                case "hex":
                    random = GetRandomHexNumber(2);
                    break;
                case "dec":
                    random = GetRandomNumber();
                    break;
                case "oct":
                    random = GetRandomOctaNumber();
                    break;
                case "bin":
                    random = GetRandomBinNumber();
                    break;
                default:
                    Console.WriteLine($"Not found in menu");
                    break;
            }

            foreach (char c in random)
            {
                Pad numberpad = new Pad();
                numberpad.SetNumberPad(c.ToString());
            }
        }

        public bool VerifyResultsMatchAllResults(string selectOperator)
        {
            string results = string.Empty;
            string selection = string.Empty;
            string binNumber = string.Empty;
            string octResult = string.Empty;
            string decimalResult = string.Empty;
            string hexResult = string.Empty;

            switch (selectOperator.ToLower())
            {
                case "hex":
                    results = GetResultsText().Replace(" ", "");
                    decimalResult = Convert.ToInt64(results, 16).ToString();
                    octResult = Convert.ToString(Int64.Parse(decimalResult), 8);
                    hexResult = Convert.ToString(Int64.Parse(decimalResult), 16).ToUpper();
                    binNumber = Convert.ToString(Int64.Parse(decimalResult), 2).PadLeft(16, '0');
                    break;
                case "dec":
                    results = GetResultsText().Replace(" ", "");
                    decimalResult = results;
                    octResult = Convert.ToString(Int64.Parse(results), 8);
                    hexResult = Convert.ToString(Int64.Parse(results), 16).ToUpper();
                    binNumber = Convert.ToString(Int64.Parse(decimalResult), 2).PadLeft(16, '0');
                    break;
                case "oct":
                    results = GetResultsText().Replace(" ", "");
                    decimalResult = Convert.ToInt64(results, 8).ToString();
                    octResult = Convert.ToString(Int64.Parse(decimalResult), 8);
                    hexResult = Convert.ToString(Int64.Parse(decimalResult), 16).ToUpper();
                    binNumber = Convert.ToString(Int64.Parse(decimalResult), 2).PadLeft(16, '0');
                    break;
                case "bin":
                    results = GetResultsText().Replace(" ", "");
                    decimalResult = Convert.ToInt64(results, 2).ToString();
                    octResult = Convert.ToString(Int64.Parse(decimalResult), 8);
                    hexResult = Convert.ToString(Int64.Parse(decimalResult), 16).ToUpper();
                    binNumber = Convert.ToString(Int64.Parse(decimalResult), 2).PadLeft(16, '0');
                    break;
                default:
                    Console.WriteLine($"Not found in menu");
                    break;
            }
            string binLeftZeros = GetResultsBaseOnType(ResultBinId);
            string binNoLeftZeros = binLeftZeros.Substring(binLeftZeros.IndexOf('1') );
            string noLeftZerosResults = binNumber.Substring(binNumber.IndexOf('1') );

            if (string.Equals(hexResult, GetResultsBaseOnType(ResultHexId))
                    && string.Equals(decimalResult, GetResultsBaseOnType(resultDecimalId))
                    && string.Equals(noLeftZerosResults, binNoLeftZeros)
                    && string.Equals(octResult, GetResultsBaseOnType(ResultOctId))
            ) return true;
            else return false;
        }

        /// <summary>
        /// Send ID of the results type
        /// </summary>
        /// <param name="selectOperator"></param>
        /// <returns></returns>
        public string GetResultsBaseOnType(string selectOperator)
        {
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement resultsElement = FindChildByClassName(childCalculatorWindow, "LandmarkTarget");
            resultsElement = FindChildById(resultsElement, "ProgrammerOperators");
            resultsElement = FindChildById(resultsElement, selectOperator);
            return resultsElement.Current.Name.Substring(resultsElement.Current.Name.IndexOf(' ') + 1).Replace(" ", ""); 
        }

        /// <summary>
        /// Get random hex number
        /// </summary>
        /// <param name="digits"></param>
        /// <returns></returns>
        private static string GetRandomHexNumber(int digits)
        {
            Random random = new Random();
            byte[] buffer = new byte[digits / 2];
            random.NextBytes(buffer);
            string result = String.Concat(buffer.Select(x => x.ToString("X2")).ToArray());
            if (digits % 2 == 0)
                return result;
            return result + random.Next(16).ToString("X");
        }

        /// <summary>
        /// Gets a random number octa
        /// </summary>
        /// <returns></returns>
        private static string GetRandomOctaNumber()
        {
             Random random = new Random();
             int  num = random.Next(600);
             return Convert.ToString(num, 8);
        }

        /// <summary>
        /// Gets a random Bin number
        /// </summary>
        /// <returns></returns>
        private static string GetRandomBinNumber()
        {
            Random random = new Random();
            string  num = random.Next(1000).ToString();
            return Convert.ToString(Int64.Parse(num), 2).PadLeft(16, '0');
        }

    }
}
