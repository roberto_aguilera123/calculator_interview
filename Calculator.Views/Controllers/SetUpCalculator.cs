﻿using Calculator.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Linq;

namespace Calculator.Views.Controllers
{
    public class SetUpCalculator
    {
        public static void StartCalculator()
        {
            var start = new ProcessStartInfo
            {
                FileName = Constant.CalculatorPath,
                UseShellExecute = false,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                Verb = "runas"      // run as admin                      
            };
            Console.WriteLine("\nBegin WPF UIAutomation test run\n");
            Console.WriteLine("Launching calculator application");
            Process process = Process.Start(start);
        }

        public static void CloseCalculator()
        {
            var start = new ProcessStartInfo
            {
                FileName = "taskkill",
                Arguments = "-f -im calculator.exe",
                UseShellExecute = false,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                Verb = "runas"      // run as admin                      
            };
            using (Process process = Process.Start(start))
            {
                if (process != null)
                    using (StreamReader reader = process.StandardOutput)
                    {
                        string cmdResult = reader.ReadToEnd();
                        Console.WriteLine("Closing calculator");
                        Thread.Sleep(500);
                        process.WaitForExit();
                    }
            }
        }
    }
}
