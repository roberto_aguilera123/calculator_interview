﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Automation;

namespace Calculator.Views.Controllers
{
    public class AutomationExtensions
    {
        /// <summary>
        /// Retrieves an element in a list, using FindAll.
        /// </summary>
        /// <param name="parent">The list element.</param>
        /// <param name="index">The index of the element to find.</param>
        /// <returns>The list item.</returns>
        public AutomationElement FindChildByName(AutomationElement parent, string name)
        {
            if ((name == "") || (parent == null))
            {
                throw new ArgumentException("Argument cannot be null or empty.");
            }
            // Set a property condition that will be used to find the main form of the
            // target application. In the case of a WinForms control, the name of the control
            // is also the AutomationId of the element representing the control.
            AutomationElement aeButton = parent.FindFirst(TreeScope.Children,
               new PropertyCondition(AutomationElement.NameProperty, name));

            // Find the element.
            return aeButton;
        }

        /// <summary>
        /// Find by child by name and class name
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="name"></param>
        /// <param name="className"></param>
        /// <returns></returns>
        public AutomationElement FindChildByNameAndClassName(AutomationElement parent, string name,string className)
        {
            AutomationElement element;

            if ((name == "") || (parent == null))
            {
                throw new ArgumentException("Argument cannot be null or empty.");
            }
            int numWaits = 0;
            do
            {
                Console.WriteLine("Looking for " + name+ "  . . . ");
                element = parent.FindFirst(TreeScope.Children,
                           new AndCondition(
                           new PropertyCondition
                            (AutomationElement.NameProperty, name),
                                           new PropertyCondition
                            (AutomationElement.ClassNameProperty, className)));
                ++numWaits;
                Thread.Sleep(100);
            } while (element == null && numWaits < 50);
            if (element == null) throw new Exception("Failed to find " + name);
            else Console.WriteLine("Found it!");
            return element;
        }

        /// <summary>
        /// Find a child element buy id
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public AutomationElement FindChildById(AutomationElement parent, string id)
        {
            AutomationElement element;

            if ((id == "") || (parent == null))
            {
                throw new ArgumentException("Argument cannot be null or empty.");
            }
            int numWaits = 0;
            do
            {
                Console.WriteLine("Looking for " + id + "  . . . ");
                element = parent.FindFirst(TreeScope.Children,
                           new PropertyCondition(AutomationElement.AutomationIdProperty, id));
                ++numWaits;
                Thread.Sleep(100);
            } while (element == null && numWaits < 50);
            if (element == null) throw new Exception("Failed to find " + id);
            else Console.WriteLine("Found it!");
            return element;
        }

        /// <summary>
        /// Finds all child element by class nane
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public AutomationElementCollection FindChildByAllClassName(AutomationElement parent, string id)
        {

            if ((id == "") || (parent == null))
            {
                throw new ArgumentException("Argument cannot be null or empty.");
            }
            Console.WriteLine("Looking for " + id + "  . . . ");
            var  element = parent.FindAll(TreeScope.Children,
           new PropertyCondition(AutomationElement.ClassNameProperty, id));
            if (element == null) throw new Exception("Failed to find " + id);
            else Console.WriteLine("Found it!");
            return element;
        }

        /// <summary>
        /// Find child element by  class name
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="className"></param>
        /// <returns></returns>
        public AutomationElement FindChildByClassName(AutomationElement parent, string className)
        {
            AutomationElement element;
            int numWaits = 0;
            do
            {
                Console.WriteLine("Looking for " + className + "  . . . ");
                element = parent.FindFirst(TreeScope.Children,
                           new PropertyCondition(AutomationElement.ClassNameProperty, className));
                ++numWaits;
                Thread.Sleep(100);
            } while (element == null && numWaits < 50);
            if (element == null) throw new Exception("Failed to find " + className);
            else Console.WriteLine("Found it!");
            return element;
        }

    }
}
