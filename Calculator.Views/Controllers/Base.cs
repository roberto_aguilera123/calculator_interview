﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Automation;

namespace Calculator.Views.Controllers
{
    public class Base : AutomationExtensions
    {
        /// <summary>
        /// Desktop elemtn
        /// </summary>
        public AutomationElement aeDesktop;
        public AutomationElement aeCalculator;

        /// <summary>
        /// Gets the calculator window
        /// </summary>
        /// <returns></returns>
        public static AutomationElement GetCalculatorWindow()
        {
            AutomationElement aeDesktop = AutomationElement.RootElement;
            AutomationElement aeForm;

            int numWaits = 0;
            do
            {
                Console.WriteLine("Looking for calculator . . . ");
                aeForm = aeDesktop.FindFirst(TreeScope.Children,
                           new AndCondition(
                           new PropertyCondition
                            (AutomationElement.NameProperty, "Calculator"),
                                           new PropertyCondition
                            (AutomationElement.ClassNameProperty, "ApplicationFrameWindow")));
                ++numWaits;
                Thread.Sleep(100);
            } while (aeForm == null && numWaits < 50);
            if (aeForm == null) throw new Exception("Failed to find calculator");
            else Console.WriteLine("Found it!");
            aeForm.SetFocus();

            return aeForm;
        }

        /// <summary>
        /// CLicks on element based on ID
        /// </summary>
        /// <param name="element"></param>
        /// <param name="itemName"></param>
        /// <returns></returns>
        public AutomationElement ClickOnElement(AutomationElement element, string itemName)
        {
            AutomationElement itemToClick = FindChildById(element, itemName);
            if (itemToClick == null)
            {
                Console.WriteLine("Add button instance not found");
            }
            else
            {
                //Button support Invoke Pattern
                InvokePattern buttonPattern =
               (InvokePattern)itemToClick.GetCurrentPattern(InvokePattern.Pattern);
                //Once get the pattern then calling Invoke method on that
                buttonPattern.Invoke();
            }
            return itemToClick;
        }
        /// <summary>
        /// Clikcs on an element by name
        /// </summary>
        /// <param name="element"></param>
        /// <param name="itemName"></param>
        /// <returns></returns>
        public AutomationElement ClickOnElementByName(AutomationElement element, string itemName)
        {
            AutomationElement itemToClick = FindChildByName(element, itemName);
            if (itemToClick == null)
            {
                Console.WriteLine("Add button instance not found");
            }
            else
            {
                //Button support Invoke Pattern
                InvokePattern buttonPattern =
               (InvokePattern)itemToClick.GetCurrentPattern(InvokePattern.Pattern);
                //Once get the pattern then calling Invoke method on that
                buttonPattern.Invoke();
            }
            return itemToClick;
        }

        /// <summary>
        /// Sets select element by id property
        /// </summary>
        /// <param name="element"></param>
        /// <param name="itemName"></param>
        /// <returns></returns>
        public void SetSelectElement(AutomationElement element, string itemText)
        {
            if ((element == null) || (itemText == ""))
            {
                throw new ArgumentException(
                    "Argument cannot be null or empty.");
            }

            Condition propertyCondition = new PropertyCondition(
                AutomationElement.AutomationIdProperty,
                itemText,
                PropertyConditionFlags.IgnoreCase);

            AutomationElement firstMatch =
                element.FindFirst(TreeScope.Children, propertyCondition);

            if (firstMatch != null)
            {
                try
                {
                    SelectionItemPattern selectionItemPattern;
                    selectionItemPattern =
                    firstMatch.GetCurrentPattern(
                    SelectionItemPattern.Pattern) as SelectionItemPattern;
                    selectionItemPattern.Select();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        /// <summary>
        /// Sets selects element by name
        /// </summary>
        /// <param name="element"></param>
        /// <param name="itemText"></param>
        public void SetSelectElementByName(AutomationElement element, string itemText)
        {
            if ((element == null) || (itemText == ""))
            {
                throw new ArgumentException(
                    "Argument cannot be null or empty.");
            }

            Condition propertyCondition = new PropertyCondition(
                AutomationElement.NameProperty,
                itemText,
                PropertyConditionFlags.IgnoreCase);

            AutomationElement firstMatch =
                element.FindFirst(TreeScope.Children, propertyCondition);

            if (firstMatch != null)
            {
                try
                {
                    SelectionItemPattern selectionItemPattern;
                    selectionItemPattern =
                    firstMatch.GetCurrentPattern(
                    SelectionItemPattern.Pattern) as SelectionItemPattern;
                    selectionItemPattern.Select();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        /// <summary>
        /// Gets the text from the results
        /// </summary>
        /// <returns></returns>
        public string GetResultsText()
        {
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement resultsElement = FindChildByClassName(childCalculatorWindow, "LandmarkTarget");
            resultsElement = FindChildById(resultsElement, "CalculatorResults");
            return resultsElement.Current.Name.Substring(11);
        }

        /// <summary>
        /// Gets the calculation exppression
        /// </summary>
        /// <returns></returns>
        public string GetCalculationExpressionText()
        {
            AutomationElement childCalculatorWindow = FindChildByNameAndClassName(aeCalculator, "Calculator", "Windows.UI.Core.CoreWindow");
            AutomationElement resultsElement = FindChildByClassName(childCalculatorWindow, "LandmarkTarget");
            resultsElement = FindChildById(resultsElement, "CalculatorExpression");
            return resultsElement.Current.Name.Substring(14); 

        }

        /// <summary>
        /// Gets a random number from 0-1000
        /// </summary>
        /// <returns></returns>
        public static string GetRandomNumber()
        {
            Random random = new Random();
            int num = random.Next(1000);
            return Convert.ToString(num);
        }
    }  
}
