﻿using Calculator.Data.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Data
{
    public class Constant
    {
        public static readonly string Timeout = Session.AppSettings.Timeout;
        public static readonly string CalculatorPath = Session.EnviromentSettings.CalculatorPath;
    }
}
