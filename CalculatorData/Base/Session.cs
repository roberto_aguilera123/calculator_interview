﻿using Calculator.Data.Models;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Data.Base
{
    class Session
    {
        /// <summary>
        /// Get or Set Appsettings propierties
        /// </summary>
        public static AppSettingsModel AppSettings { get; set; }

        /// <summary>
        /// Get or Set Enviroment propierties
        /// </summary>
        public static EnvironmentSettingsModel EnviromentSettings { get; set; }


        /// <summary>
        /// Initialize the configuration once
        /// </summary>
        static Session()
        {
            GetSessionConfiguration();
        }

        /// <summary>
        /// Retrieves Session configuration
        /// </summary>
        private static void GetSessionConfiguration()
        {
            try
            {
                Session.AppSettings = GetAppSettings();
                Session.EnviromentSettings = GetEnviromentSettings();
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// Set the Url from the Json configurations
        /// </summary>
        /// <returns></returns>
        private static EnvironmentSettingsModel GetEnviromentSettings()
        {
            try
            {
                EnvironmentSettingsModel environmentSettings = new EnvironmentSettingsModel();
                var builder = new ConfigurationBuilder().SetBasePath($"{AppDomain.CurrentDomain.BaseDirectory}")
                    .AddJsonFile($"EnvironmentsJson/{Session.AppSettings.Environment}.json");
                var settings = builder.Build();
                environmentSettings = settings.GetSection("EnvironmentSettings").Get<EnvironmentSettingsModel>();
                return environmentSettings;

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Set the app settings from the runsettings file
        /// </summary>
        /// <returns></returns>
        private static AppSettingsModel GetAppSettings()
        {
            try
            {
                AppSettingsModel appSettings = new AppSettingsModel
                {
                    Environment = TestContext.Parameters.Get("TestEnvironment"),
                    Timeout = TestContext.Parameters.Get("Timeout"),
                };

                return appSettings;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
