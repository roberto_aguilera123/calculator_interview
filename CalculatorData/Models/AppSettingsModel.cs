﻿namespace Calculator.Data.Models
{
    /// <summary>
    /// Env setting to be read from the runsettings file
    /// </summary>
    public class AppSettingsModel

    {
        /// <summary>
        /// Sets the enviroment Release or Dev for CI/CD
        /// </summary>
        public string Environment { get; set; }

        /// <summary>
        /// Sets timeout to finds elemetn Release or Dev for CI/CD
        /// </summary>
        public string Timeout { get; set; }
    }
}
