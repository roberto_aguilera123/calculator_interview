﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Data.Models
{
    /// <summary>
    /// Env setting to be read from the JSOn conig file
    /// </summary>
    public class EnvironmentSettingsModel
    {

        /// <summary>
        /// Sets the path of the calculator for the OS
        /// </summary>
        public string CalculatorPath { get; set; }


    }
}
