﻿using Calculator.Views.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace Calculator.UI.Hooks
{
    [Binding]
    public sealed class HooksCalculator
    {
        // For additional details on SpecFlow hooks see http://go.specflow.org/doc-hooks

        /// <summary>
        /// Open calculator
        /// </summary>
        [BeforeScenario]
        public void BeforeScenario()
        {
            SetUpCalculator.CloseCalculator();
            SetUpCalculator.StartCalculator();
        }

        /// <summary>
        /// Close calculator
        /// </summary>
        [AfterScenario]
        public void AfterScenario()
        {
            SetUpCalculator.CloseCalculator();

        }
    }
}
