﻿Feature: StandardCalculatorFeature
Background: 	
	Given calculator is in standard mode
	And perform a sum of two random values

@d
Scenario: Cancel Operation and retrieve from history
	When user clears standard calculation
	And retrieves operation from history
	Then verify history match random sum at begin

@d
Scenario: Verify some memory additional
	When calculator saves result in memory
	Then click memory add and verify result
@d
Scenario: Verify some memory  retrieval.
	When calculator saves result in memory
	And clears display result
	And retrieves value from memory
	Then  verify value retrieved is correct