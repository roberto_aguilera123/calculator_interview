﻿Feature: Calculator

@A
Scenario Outline: Verify programmer mode values HEX, DEC, OCT, and BIN
	Given calculator is in programmer mode
	When sets value in <typeOfData> configuration
	Then verify values are  HEX, DEC, OCT, and BIN are correct based on <typeOfData>
Examples: 
| typeOfData |
|  HEX       |
|  OCT       |
|  BIN       |
|  DEC       |

@B
Scenario: Launch calc, navigate to scientific mode, perform log 10 base calculation
	Given calculator is in scientific mode
	When calculator performs a log 10 base calculation of a random number 
	Then verify calculation results are correct


@C
Scenario Outline: Launch calc, navigate to date calculation mode, enter dates and verify day and week results
	Given calculator is in date calculation mode
	And calculator date calculation is in difference between dates
	When the amount of days <days> are added to todays date
	Then verify display date calcuation difference is correct
Examples: 
| days  |
| 5     |
| 10    |
| 32    |
| 500   |
