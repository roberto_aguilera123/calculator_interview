﻿using Calculator.Views.Menu;
using Calculator.Views.Menus;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace Calculator.UI.Steps
{
    [Binding]
    public sealed class StandardCalculatorSteps
    {

        private NavigationMenu navigationMenu;
        private ScenarioContext _scenarioContext;
        private StandardCalculatorMenuView standardCalculatorMenu;
        public StandardCalculatorSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
            navigationMenu = new NavigationMenu();
            standardCalculatorMenu = new StandardCalculatorMenuView();
        }

        [Given(@"calculator is in standard mode")]
        public void GivenCalculatorIsInStandardMode()
        {
            navigationMenu.GoToStandard();
        }

        [Given(@"perform a sum of two random values")]
        public void GivenPerfromASumOfTwoRandomValues()
        {
            Random random = new Random();
            int a = random.Next(9999999);
            int b = random.Next(1000);
            int result = standardCalculatorMenu.SumTwoNumbers(a,b);
            _scenarioContext.Add("a",a);
            _scenarioContext.Add("b", b);
            _scenarioContext.Add("result", result);
        }

        [When(@"user clears standard calculation")]
        public void WhenUserClearsStandardCalculation()
        {
            standardCalculatorMenu.ClearCalculator();
        }

        [When(@"retrieves operation from history")]
        public void WhenRetrievesOperationFromHistory()
        {
            standardCalculatorMenu.ClickFirstItemFromHistory();
        }

        [Then(@"verify history match random sum at begin")]
        public void ThenVerifyHistoryMatchRandomSumAtBegin()
        {
            string a = _scenarioContext["a"].ToString();
            string b = _scenarioContext["b"].ToString();
            string result = _scenarioContext["result"].ToString();
            Assert.IsTrue(standardCalculatorMenu.DoesDisplayResultContains(a, b, result));
        }

        [When(@"calculator saves result in memory")]
        public void WhenCalculatorSavesResultInMemory()
        {
            standardCalculatorMenu.SaveInMemory();
        }

        [Then(@"click memory add and verify result")]
        public void ThenClickMemoryAddAndVerifyResult()
        {
            string memoryValue = standardCalculatorMenu.GetMemoryValue();
            standardCalculatorMenu.ClickMemoryAdd();
            string memoryValueAfterAdd = standardCalculatorMenu.GetMemoryValue();
            string doubleMemoryValue = (Convert.ToInt64(memoryValue) + Convert.ToInt64(memoryValue)).ToString();
            Assert.IsTrue(String.Equals(memoryValueAfterAdd, doubleMemoryValue));
        }

        [When(@"clears display result")]
        public void WhenClearsDisplayResult()
        {
            standardCalculatorMenu.ClearCalculator();
            string displayClear = standardCalculatorMenu.GetResultsText().Replace(",","");
            _scenarioContext.Add("displayClear", displayClear);

        }

        [When(@"retrieves value from memory")]
        public void WhenRetrievesValueFromMemory()
        {
            standardCalculatorMenu.RetrieveValueFromMemory();
        }

        [Then(@"verify value retrieved is correct")]
        public void ThenVerifyValueRetrievedIsCorrect()
        {
            string displayResult = standardCalculatorMenu.GetResultsText().Replace(",", "");
            Assert.IsFalse(string.Equals(displayResult, _scenarioContext["displayClear"].ToString()));
        }









    }
}
