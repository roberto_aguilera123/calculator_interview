﻿using Calculator.Views.Menu;
using Calculator.Views.Menus;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace Calculator.UITests.Steps
{
    [Binding]
    public sealed class CalculatorSteps
    {

        private NavigationMenu navigationMenu;
        private ProgrammerMenuVIew programmerMenu;
        private ScientificMenuView scientificMenu;
        private DateCalculationMenuView dateCalculationMenu;
        private ScenarioContext _scenarioContext;
        public CalculatorSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
            navigationMenu = new NavigationMenu();
            programmerMenu = new ProgrammerMenuVIew();
            scientificMenu = new ScientificMenuView();
            dateCalculationMenu = new DateCalculationMenuView();
        }

        [Given(@"calculator is in programmer mode")]
        public void GivenCalculatorIsInProgrammerMode()
        {
            navigationMenu.GoToProgrammerMode();
        }

        [When(@"sets value in (.*) configuration")]
        public void WhenSetsValueInConfiguration(string typeOfData)
        {
            programmerMenu.SetOperator(typeOfData);
            programmerMenu.SetRandomNumberBasedOnOperator(typeOfData);

        }

        [Then(@"verify values are  HEX, DEC, OCT, and BIN are correct based on (.*)")]
        public void ThenVerifyValuesAreHEXDECOCTAndBINAreCorrectBaedOnHEX(string typeOfData)
        {
          Assert.IsTrue( programmerMenu.VerifyResultsMatchAllResults(typeOfData),"The values are not the same");
        }

        [Given(@"calculator is in scientific mode")]
        public void GivenCalculatorIsInScientificMode()
        {
            navigationMenu.GoToScientificMode();
        }

        [When(@"calculator performs a log 10 base calculation of a random number")]
        public void WhenCalculatorPerformsALogCalculationOfARandomNumber()
        {
            Random random = new Random();
            int num = random.Next(1000);
            string result =  scientificMenu.CalculateBaseLogFroMNumber(num.ToString());
            _scenarioContext.Add("DisplayResult", result);
            _scenarioContext.Add("Number", num);
        }

        [Then(@"verify calculation results are correct")]
        public void ThenVerifyCalculationResultsAreCorrect()
        {
          Assert.IsTrue(scientificMenu.IsBasicLogResultsDisplayCorrect(_scenarioContext["Number"].ToString(),
                            _scenarioContext["DisplayResult"].ToString()),"Log base 10 results are not the same");
        }

        [Given(@"calculator is in date calculation mode")]
        public void GivenCalculatorIsInDateCalculationMode()
        {
            navigationMenu.GoToDateCalcuation();
        }

        [Given(@"calculator date calculation is in difference between dates")]
        public void GivenCalculatorDateCalculationIsInDifferenceBetweenDates()
        {
            dateCalculationMenu.SetDifferenceBetweenDates();

        }

        [When(@"the amount of days (.*) are added to todays date")]
        public void WhenTheAmountOfDaysAreAddedToTodaysDate(int days)
        {
            DateTime thisDay = DateTime.Today;
            dateCalculationMenu.SetDateFrom(thisDay);
            dateCalculationMenu.SetDateTo(thisDay.AddDays(days));
            _scenarioContext.Add("daysDiff", days);
        }

        [Then(@"verify display date calcuation difference is correct")]
        public void ThenVerifyDisplayDateCalcuationDifferenceIsCorrect()
        {
            dateCalculationMenu.IsDateDifferenceCorrect(Convert.ToInt32(_scenarioContext["daysDiff"]));
        }











    }
}
